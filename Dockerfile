FROM php:7.2-apache-stretch

ENV APACHE_DOCUMENT_ROOT /var/www/html

# Install package
RUN apt-get update && apt-get install -y \
    vim \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libjpeg-dev \
    zlib1g-dev \
    libzip-dev \
    libgmp-dev \
    libldap2-dev \
    libmcrypt-dev \
    zlib1g-dev \
    libicu-dev \
    libxslt-dev \
    libxml2-dev \
    gettext-base \
    iputils-ping \
    iproute2 \
    netcat \
    telnet \
    g++ vim gnupg make git zsh zip unzip cron \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Magento Php 7.2 need libsodium
RUN curl -O https://download.libsodium.org/libsodium/releases/libsodium-1.0.18.tar.gz \
  && tar xfvz libsodium-1.0.18.tar.gz \
  && cd libsodium-1.0.18 \
  && ./configure \
  && make && make install \
  && cd ../ && rm -rf libsodium-1.0.18 libsodium-1.0.18.tar.gz

# Config PHP module
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-configure intl

# Install PHP module
RUN docker-php-ext-install -j$(nproc) iconv gd

RUN docker-php-ext-install \
    pdo \
    pdo_mysql \
    zip \
    bcmath \
    intl \
    xsl \
    soap \
    pcntl \
    opcache

RUN pecl install \
    redis-5.0.2 \
    xdebug-2.7.2 \
    libsodium \
    apcu \
    apcu_bc-1.0.5

RUN docker-php-ext-enable \
    sodium \
    redis \
    xdebug \
    apcu --ini-name 10-docker-php-ext-apcu.ini \
    apc --ini-name 20-docker-php-ext-apc.ini

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY ./docker/auth.json /root/.composer

#RUN composer create-project --repository=https://repo.magento.com/ magento/project-community-edition /var/www/html/

#COPY . /var/www/html/

WORKDIR ${APACHE_DOCUMENT_ROOT}

# Add Permission
RUN chmod -R 777 var/ pub/ generated/ app/

# Enabled Rewrite mode. It's important
RUN a2enmod rewrite ssl