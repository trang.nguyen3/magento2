<?php
return [
    'backend' => [
        'frontName' => 'admin'
    ],
    'db' => [
        'table_prefix' => '',
        'connection' => [
            'default' => [
                'host' => getenv('MARIADB_HOST') .':'. getenv('MARIADB_PORT_NUMBER'),
                'dbname' => getenv('MARIADB_DATABASE_NAME'),
                'username' => getenv('MARIADB_DATABASE_USERNAME'),
                'password' => getenv('MARIADB_DATABASE_PASSWORD'),
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;',
                'active' => '1'
            ]
        ]
    ],
    'cache_types' => [
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'compiled_config' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'full_page' => 1,
        'config_webservice' => 1,
        'translate' => 1,
        'vertex' => 1
    ],
    'crypt' => [
        'key' => '1945dbbdd19a332c75e7437492b315c4'
    ],

    'install' => [
        'date' => 'Thu, 06 Feb 2019 09:31:07 +0000'
    ],
    'MAGE_MODE' => 'developer',
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'session' => [
        'save' => 'files'
    ],
    'x-frame-options' => 'SAMEORIGIN',
];

